const equalsIC = (str1, str2) => {
  return new RegExp(`^${str1}$`, "i").test(str2);
};
if (String.prototype.ignoreCaseEqual) {
    throw "From `string-equal` library: `String.prototype.ignoreCaseEqual()` alredy exist. Probably you have included some library used this function. Please try to remove the library that is using it. Or be sure you are not writing this function on your on in your awesome code."
}
if (String.prototype.equal) {
    throw "From `string-equal` library: `String.prototype.equal()` alredy exist. Probably you have included some library used this function. Please try to remove the library that is using it. Or be sure you are not writing this function on your on in your awesome code."
}
String.prototype.ignoreCaseEqual = function (str) {
  // Ignoring case
  if (equalsIC(this.valueOf(), str.valueOf())) {
    return true;
  }
  return false;
};
String.prototype.equal = function (str) {
  // Case sensitive
  if (this.valueOf() === str.valueOf()) {
    return true;
  }
  return false;
};