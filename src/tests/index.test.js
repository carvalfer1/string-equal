String.prototype.ignoreCaseEqual = () => {
    console.log("Hola mundo")
}
require('../index.js');
describe("testing string-equals functionality", () => {
    it("ignoreCaseEqual() -> given a string = 'XX' and b string = 'xx' must return true", ()=>{
        expect("XX".ignoreCaseEqual("xx")).toEqual(true);
    });
    it("ignoreCaseEqual() -> given a string = 'XX' and b string = 'XX' must return true", ()=>{
        expect("XX".ignoreCaseEqual("XX")).toEqual(true);
    });
    it("ignoreCaseEqual() -> given a string = 'xx' and b string = 'xx' must return true", ()=>{
        expect("xx".ignoreCaseEqual("xx")).toEqual(true);
    });
    it("ignoreCaseEqual() -> given a string = 'xx' and b string = 'XX' must return true", ()=>{
        expect("xx".ignoreCaseEqual("XX")).toEqual(true);
    });
    it("ignoreCaseEqual() -> given a string = 'xx' and b string = 'yy' must return false", ()=>{
        expect("xx".ignoreCaseEqual("yy")).toEqual(false);
    });
    it("ignoreCaseEqual() -> given a string = 'XX' and b string = 'yy' must return false", ()=>{
        expect("XX".ignoreCaseEqual("yy")).toEqual(false);
    });
    it("equal() -> given a string = 'XX' and b string = 'xx' must return false", ()=>{
        expect("XX".equal("xx")).toEqual(false);
    });
    it("equal() -> given a string = 'XX' and b string = 'XX' must return true", ()=>{
        expect("XX".equal("XX")).toEqual(true);
    });
    it("equal() -> given a string = 'xx' and b string = 'xx' must return true", ()=>{
        expect("xx".equal("xx")).toEqual(true);
    });
    it("equal() -> given a string = 'xx' and b string = 'XX' must return false", ()=>{
        expect("xx".equal("XX")).toEqual(false);
    });
    it("equal() -> given a string = 'xx' and b string = 'yy' must return false", ()=>{
        expect("xx".equal("yy")).toEqual(false);
    });
    it("equal() -> given a string = 'XX' and b string = 'yy' must return false", ()=>{
        expect("XX".equal("yy")).toEqual(false);
    });
})